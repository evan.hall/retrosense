import matplotlib as mpl
import numpy as np

from matplotlib import pyplot as plt
from scipy import constants as const


# %%
datadir = '../data/2024/02'
figdir = '../figures'

# %%
lambda_ = 850e-9 # m

Rt = 5e3 # V/A

x = np.array([
    0.865, 0.860, 0.855, 0.850, 0.845,
    0.840, 0.835, 0.834, 0.833, 0.832,
    0.831, 0.830, 0.829, 0.828, 0.827,
    0.826, 0.825, 0.820, 0.815, 0.810,
    0.805, 0.800, 0.795,
]) - 0.830 # inches
x *= 25.4 # mm

Vx = np.array([
    1.83, 2.87, 3.84, 4.68, 5.06,
    4.09, 2.28, 1.84, 1.45, 0.99,
    0.57, 0.15, -0.03, -0.07, -1.18,
    -1.56, -1.97, -3.76, -4.87, -4.64,
    -3.87, -2.92, -1.93,
]) # V
Ix = Vx / Rt

x = np.array([
    865, 860, 855,
    850, 845, 840, 839, 838,
    837, 836, 835, 834, 833,
    832, 831, 830, 829, 828,
    827, 826, 825, 824, 823,
    822, 821, 820, 815, 810,
    805, 800, 795,
],
dtype=float) - 830 # mils
x *= 25.4e-3 # mm
xunc = 0.1 * 25.4e-3 # mm

Vx = np.array([
    1.87, 2.86, 3.86,
    4.69, 5.05, 4.14, 3.81, 3.45,
    3.08, 2.79, 2.36, 1.89, 1.42,
    1.09, 0.67, 0.24, -0.19, -0.65,
    -1.04, -1.45, -1.86, -2.26, -2.66,
    -3.02, -3.35, -3.68, -4.78, -4.54,
    -3.80, -2.87, -1.83,
])
Ix = Vx / Rt
Iunc = 0.03 / Rt

Vy = np.array([
    0.13, 0.08, 0.02,
    -0.02, -0.02, 0.04, 0.04, 0.03,
    0.03, 0.03, 0.02, 0.00, 0.00,
    -0.01, -0.01, -0.03, -0.04, -0.06,
    -0.07, -0.08, -0.09, -0.10, -0.12,
    -0.14, -0.15, -0.17, -0.30, -0.36,
    -0.30, -0.19, -0.08,
])
Iy = Vy / Rt

Vs = np.array([
    1.88, 2.97, 4.11,
    5.19, 5.94, 6.27, 6.31, 6.33,
    6.36, 6.38, 6.39, 6.41, 6.42,
    6.42, 6.42, 6.42, 6.42, 6.41,
    6.41, 6.40, 6.38, 6.35, 6.33,
    6.30, 6.27, 6.23, 5.94, 5.21,
    4.22, 3.06, 1.99,
])
Is = Vs / Rt

# %%

px = np.polyfit(x[5:-5], Ix[5:-5], 1)
py = np.polyfit(x[5:-5], Iy[5:-5], 1)
pn = np.polyfit(x[5:-5], Ix[5:-5] / Is[5:-5], 1)
# %%
fig, axs = plt.subplots(
    2,
    1,
    figsize=(6, 8),
    sharex=True,
    gridspec_kw={'height_ratios': [3, 1]})
axs[0].errorbar(
    x, 
    1e3 * Ix,
    yerr=1e3 * Iunc,
    xerr=xunc,
    fmt='.',
    label='$I_X$',
    color='xkcd:steel blue',
    alpha=0.9,
    )
axs[0].errorbar(
    x, 
    1e3 * Iy,
    yerr=1e3 * Iunc,
    xerr=xunc,
    fmt='.',
    label='$I_Y$',
    color='xkcd:goldenrod',
    alpha=0.9,
    )
axs[0].errorbar(
    x, 
    1e3 * Is,
    yerr=1e3 * Iunc,
    xerr=xunc,
    fmt='.',
    label=r'$I_\mathrm{sum}$',
    color='xkcd:dark rose',
    alpha=0.9,
    )
axs[0].errorbar(
    x, 
    Ix / Is,
    yerr=1e3 * Iunc,
    xerr=xunc,
    fmt='.',
    label=r'$I_X / I_\mathrm{sum}$',
    color='xkcd:dull green',
    )
axs[0].plot(
    x[5:-5],
    1e3 * np.polyval(px, x[5:-5]),
    color='xkcd:cyan',
)
axs[0].plot(
    x[5:-5],
    1e3 * np.polyval(py, x[5:-5]),
    color='xkcd:canary yellow',
)
axs[0].plot(
    x[5:-5],
    np.polyval(pn, x[5:-5]),
    color='xkcd:neon green',
)
axs[1].errorbar(
    x[5:-5],
    1e3 * (np.polyval(px, x[5:-5]) - Ix[5:-5]),
    yerr=1e3 * Iunc,
    xerr=xunc,
    fmt='.',
    color='xkcd:steel blue',
    )
axs[1].errorbar(
    x[5:-5],
    1e3 * (np.polyval(py, x[5:-5]) - Iy[5:-5]),
    yerr=1e3 * Iunc,
    xerr=xunc,
    fmt='.',
    color='xkcd:goldenrod',
    )
axs[1].errorbar(
    x[5:-5],
    np.polyval(pn, x[5:-5]) - Ix[5:-5] / Is[5:-5],
    yerr=1e3 * Iunc,
    xerr=xunc,
    fmt='.',
    color='xkcd:dull green',
    )
axs[0].legend(loc='lower right').set_zorder(2)

axs[0].xaxis.set_major_locator(mpl.ticker.MultipleLocator(0.2))
axs[0].yaxis.set_major_locator(mpl.ticker.MultipleLocator(0.2))
axs[1].yaxis.set_major_locator(mpl.ticker.MultipleLocator(0.01))
axs[1].tick_params(which='major', axis='x', labelrotation=60)

axs[0].set_ylabel('Photocurrent [mA]')
axs[1].set_ylabel('Residual [mA]')
axs[1].set_xlabel('$X$ displacement [mm]')

fig.savefig(f'{figdir}/retrosense_linearity_2024-02.pdf')

# %%
# Now extract values
I0 = np.max(Is) # A
print(I0)
g0 = px[0] * 1e3 # gain in A/m
print(g0)
n_shot = lambda f: np.ones_like(f) * np.sqrt(2 * const.Planck * const.c / lambda_ * I0) / g0

# %%

fig_i, ax_i = plt.subplots()
xarr = (np.array([840, 835, 830, 825, 820]) - 830) * 25.4e-3 # mm
clist = plt.cm.ocean(np.linspace(0, 1, xarr.shape[0] + 2))
cmap_heat = mpl.colors.LinearSegmentedColormap.from_list("heat", ['xkcd:'+c for c in ["green", "sea blue", "dark blue", "sea blue", "green"]], N=256)
clist = cmap_heat(np.linspace(0, 256, xarr.shape[0], dtype=int))

Xrms_arr = np.zeros_like(xarr)

for ii, xx in enumerate(xarr):
    f, a = np.loadtxt(f'{datadir}/SCRN060{ii}.TXT', unpack=1)[:,3:]
    a /=  (Rt * px[0] * 1e3)
    ax_i.loglog(
        f,
        a,
        lw=2,
        c=clist[ii],
        alpha=0.8,
        label='${:+.2f}$ mm'.format(xx),
        zorder=100 + ii,
        )
    Xrms_arr[ii] = np.sqrt(np.mean(a[12:16]**2))
fd, ad = np.loadtxt(f'{datadir}/s_dark.txt', unpack=1)[:,3:]

n_bosem = lambda f: 4e-11 * np.sqrt(1 + 60/f)
n_ebosem = lambda f: np.ones_like(f) * 4e-11

ax_i.loglog(
    fd,
    ad / (Rt * px[0] * 1e3),
    lw=2,
    c='xkcd:pale purple',
    label='Dark',
    alpha=0.8,
    )
ax_i.loglog(
    f,
    n_shot(f),
    c='xkcd:orange',
    alpha=0.9,
    label=f'Shot, {I0*1e3:.1f} mA',
)
ax_i.loglog(
    f,
    n_bosem(f),
    color='xkcd:slate gray',
    ls=(0, (6, 2,)),
    alpha=0.9,
    label='BOSEM',
    zorder=20,
    )
ax_i.loglog(
    f,
    n_ebosem(f),
    c='xkcd:slate gray',
    ls=(0, (2, 1,)),
    alpha=0.9,
    label='eBOSEM',
    zorder=22,
    )

ax_i.set_xlim([f[0], f[-1]])
ax_i.set_ylim([5e-12, 2e-9])
ax_i.xaxis.set_major_formatter(
    mpl.ticker.FuncFormatter(lambda y, _: '{:g}'.format(y))
)
ax_i.set_xlabel('Frequency [Hz]')
ax_i.set_ylabel(
    r'Displacement $\left[\mathrm{m}/\sqrt{\mathrm{Hz}}\right]$'
    )
#ax_i.set_title('Noise versus $X$ offset')
ax_i.legend(
    ncol=2,
    fontsize='small',
    labelspacing=0.2,
    columnspacing=1.0,
    )

fig_i.savefig(f'{figdir}/retrosense_noise_offset_2024-02.pdf')
# %%
